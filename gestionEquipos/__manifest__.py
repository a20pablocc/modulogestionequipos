# -*- coding: utf-8 -*-
{
    'name': "Gestión equipos",  # Module title
    'summary': "Gestiona las estadísticas de tus jugadores",  # Module subtitle phrase
    'description': """Long description""",  # You can also rst format
    'author': "Pablo Costa",
    'website': "http://www.example.com",
    'category': 'Uncategorized',
    'version': '1.0.0',
    'depends': ['base'],
    # This data files will be loaded at the installation (commented becaues file is not added in this example)
    'data': [
        'security/groups.xml',
        'security/ir.model.access.csv',
        'views/equipo_jugadores.xml',
        'views/jugadores_titulares.xml',
        'views/jugadores_personal.xml',
    ],
    # This demo data files will be loaded if db initialize with demo data (commented becaues file is not added in this example)
    # 'demo': [
    #     'demo.xml'
    # ],
}
